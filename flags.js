//Path worker js

const functionRecibeHoy = function () {
var workerPath = '/static/RDF/site/home/promohuincha/html/scripts/workers_flags.js'
var imageDesktopModal = 'https://www.falabella.com.co/static/RDF/site/home/promohuincha/html/scripts/img/recibehoy/Recibehoy-desk.png'
var imageImageModal = 'https://www.falabella.com.co/static/RDF/site/home/promohuincha/html/scripts/img/recibehoy/Recibehoy-mob.gif'
//var htmlFlagInPLPAndPDP = 'Recibe <span>Hoy</span>'
var htmlFlagInPLPAndPDP = `<img src="/static/RDF/site/home/promohuincha/html/scripts/img/Icono_RecibeHoy.png" class="fb-img-express-recibe-entrega-hoy">`
var htmlFlagInPLPAndPDPventi = `<img src="/static/RDF/site/home/promohuincha/html/scripts/img/Icono_28Horas.png" class="fb-img-express-recibe-entrega-hoy">`

var delivery_in_barranquilla_cali = `<img src="/static/RDF/site/home/promohuincha/html/scripts/img/ciudades/Entrega_Barranquilla_Cali.png" class="fb-img-express-recibe-entrega-hoy">`
var delivery_in_barranquilla = `<img src="/static/RDF/site/home/promohuincha/html/scripts/img/ciudades/Entrega_Barranquilla.png" class="fb-img-express-recibe-entrega-hoy">`
var delivery_in_barranquilla_cali_medellin = `<img src="/static/RDF/site/home/promohuincha/html/scripts/img/ciudades/Entrega_Cali_Barra_Made.png" class="fb-img-express-recibe-entrega-hoy">`
var delivery_in_cali_medellin = `<img src="/static/RDF/site/home/promohuincha/html/scripts/img/ciudades/Entrega_Cali_Medellin.png" class="fb-img-express-recibe-entrega-hoy">`
var delivery_in_medellin = `<img src="/static/RDF/site/home/promohuincha/html/scripts/img/ciudades/Entrega_Medellin.png" class="fb-img-express-recibe-entrega-hoy">`
var delivery_in_barranquilla_medellin = `<img src="/static/RDF/site/home/promohuincha/html/scripts/img/ciudades/Entrega_Medellin_barra.png" class="fb-img-express-recibe-entrega-hoy">`
var delivery_in_cali = `<img src="/static/RDF/site/home/promohuincha/html/scripts/img/ciudades/Entrega_cali.png" class="fb-img-express-recibe-entrega-hoy">`

//run code if Category Page
if (window.location.href.indexOf('/category/') != -1) {
  //run code if browser support Worker API
  if (typeof Worker !== 'undefined') {
    // forEach method, could be shipped as part of an Object Literal/Module
    var forEach = function (array, callback, scope) {
      for (var i = 0; i < array.length; i++) {
        callback.call(scope, i, array[i]); // passes back stuff we need
      }
    }

    var queryStringHastaLasTres = window.location.href;
    var workerHastalasTres = new Worker(workerPath)
    var timeHastaLasTres = new Date()
    //if (timeHastaLasTres.getHours() >= 7 && timeHastaLasTres.getHours() < 17) {
      //if (timeHastaLasTres.getDay() <= 5) {

        var startWorkerHastaLasTres = function () {
          
          var arraySkusHastalasTres = []
          var fbPodItemCompareInput = document.querySelectorAll('.fb-pod__item__compare__input')
          forEach(fbPodItemCompareInput, function (index, value) {
            arraySkusHastalasTres.push(value.value)
          });
          //console.log("hasta las tres", arraySkusHastalasTres)
          workerHastalasTres.postMessage({ msg: arraySkusHastalasTres })

          workerHastalasTres.onmessage = function(event) {
            //console.log("hasta las tres entregado", event.data)
            var timeValide = new Date()
            if (true) {
              if (true) {
                if (event.data.delivery_today){
                  event.data.delivery_today.map(function(index) {
                    var divFlag = document.createElement('div')
                    divFlag.className = 'fb-flag-entrega--en-venticuadro-horas'
                    divFlag.innerHTML = htmlFlagInPLPAndPDP
                    var parentSkusDOM = document.querySelector('#fb-pod__item__input-'+ index).parentNode
                    var podItem = parentSkusDOM.parentNode
                    var sectionPodTop = podItem.querySelector('.pod-body .section__pod-top')
                    var existFlagElement = sectionPodTop.querySelector('.fb-flag-entrega--en-venticuadro-horas')
                    //console.log('existFlagElement',existFlagElement)
                    if (!existFlagElement) {
                      sectionPodTop.insertBefore(divFlag, sectionPodTop.firstChild)
                    }
                  })
                }
              }
            }
            

            if (event.data.delivery_in_barranquilla_cali){
              event.data.delivery_in_barranquilla_cali.map(function(index) {
                var divFlag = document.createElement('div')
                divFlag.className = 'fb-flag-entrega--en-venticuadro-horas'
                divFlag.innerHTML = delivery_in_barranquilla_cali
                var parentSkusDOM = document.querySelector('#fb-pod__item__input-'+ index).parentNode
                var podItem = parentSkusDOM.parentNode
                var sectionPodTop = podItem.querySelector('.pod-body .section__pod-top')
                var existFlagElement = sectionPodTop.querySelector('.fb-flag-entrega--en-venticuadro-horas')
                //console.log('existFlagElement',existFlagElement)
                if (!existFlagElement) {
                  sectionPodTop.insertBefore(divFlag, sectionPodTop.firstChild)
                }
              })
            }


            if (event.data.delivery_in_barranquilla){
              event.data.delivery_in_barranquilla.map(function(index) {
                var divFlag = document.createElement('div')
                divFlag.className = 'fb-flag-entrega--en-venticuadro-horas'
                divFlag.innerHTML = delivery_in_barranquilla
                var parentSkusDOM = document.querySelector('#fb-pod__item__input-'+ index).parentNode
                var podItem = parentSkusDOM.parentNode
                var sectionPodTop = podItem.querySelector('.pod-body .section__pod-top')
                var existFlagElement = sectionPodTop.querySelector('.fb-flag-entrega--en-venticuadro-horas')
                //console.log('existFlagElement',existFlagElement)
                if (!existFlagElement) {
                  sectionPodTop.insertBefore(divFlag, sectionPodTop.firstChild)
                }
              })
            }


            if (event.data.delivery_in_barranquilla_cali_medellin){
              event.data.delivery_in_barranquilla_cali_medellin.map(function(index) {
                var divFlag = document.createElement('div')
                divFlag.className = 'fb-flag-entrega--en-venticuadro-horas'
                divFlag.innerHTML = delivery_in_barranquilla_cali_medellin
                var parentSkusDOM = document.querySelector('#fb-pod__item__input-'+ index).parentNode
                var podItem = parentSkusDOM.parentNode
                var sectionPodTop = podItem.querySelector('.pod-body .section__pod-top')
                var existFlagElement = sectionPodTop.querySelector('.fb-flag-entrega--en-venticuadro-horas')
                //console.log('existFlagElement',existFlagElement)
                if (!existFlagElement) {
                  sectionPodTop.insertBefore(divFlag, sectionPodTop.firstChild)
                }
              })
            }


            if (event.data.delivery_in_cali_medellin){
              event.data.delivery_in_cali_medellin.map(function(index) {
                var divFlag = document.createElement('div')
                divFlag.className = 'fb-flag-entrega--en-venticuadro-horas'
                divFlag.innerHTML = delivery_in_cali_medellin
                var parentSkusDOM = document.querySelector('#fb-pod__item__input-'+ index).parentNode
                var podItem = parentSkusDOM.parentNode
                var sectionPodTop = podItem.querySelector('.pod-body .section__pod-top')
                var existFlagElement = sectionPodTop.querySelector('.fb-flag-entrega--en-venticuadro-horas')
                //console.log('existFlagElement',existFlagElement)
                if (!existFlagElement) {
                  sectionPodTop.insertBefore(divFlag, sectionPodTop.firstChild)
                }
              })
            }


            if (event.data.delivery_in_medellin){
              event.data.delivery_in_medellin.map(function(index) {
                var divFlag = document.createElement('div')
                divFlag.className = 'fb-flag-entrega--en-venticuadro-horas'
                divFlag.innerHTML = delivery_in_medellin
                var parentSkusDOM = document.querySelector('#fb-pod__item__input-'+ index).parentNode
                var podItem = parentSkusDOM.parentNode
                var sectionPodTop = podItem.querySelector('.pod-body .section__pod-top')
                var existFlagElement = sectionPodTop.querySelector('.fb-flag-entrega--en-venticuadro-horas')
                //console.log('existFlagElement',existFlagElement)
                if (!existFlagElement) {
                  sectionPodTop.insertBefore(divFlag, sectionPodTop.firstChild)
                }
              })
            }

            if (event.data.delivery_in_barranquilla_medellin){
              event.data.delivery_in_barranquilla_medellin.map(function(index) {
                var divFlag = document.createElement('div')
                divFlag.className = 'fb-flag-entrega--en-venticuadro-horas'
                divFlag.innerHTML = delivery_in_barranquilla_medellin
                var parentSkusDOM = document.querySelector('#fb-pod__item__input-'+ index).parentNode
                var podItem = parentSkusDOM.parentNode
                var sectionPodTop = podItem.querySelector('.pod-body .section__pod-top')
                var existFlagElement = sectionPodTop.querySelector('.fb-flag-entrega--en-venticuadro-horas')
                //console.log('existFlagElement',existFlagElement)
                if (!existFlagElement) {
                  sectionPodTop.insertBefore(divFlag, sectionPodTop.firstChild)
                }
              })
            }


            if (event.data.delivery_in_cali){
              event.data.delivery_in_cali.map(function(index) {
                var divFlag = document.createElement('div')
                divFlag.className = 'fb-flag-entrega--en-venticuadro-horas'
                divFlag.innerHTML = delivery_in_cali
                var parentSkusDOM = document.querySelector('#fb-pod__item__input-'+ index).parentNode
                var podItem = parentSkusDOM.parentNode
                var sectionPodTop = podItem.querySelector('.pod-body .section__pod-top')
                var existFlagElement = sectionPodTop.querySelector('.fb-flag-entrega--en-venticuadro-horas')
                //console.log('existFlagElement',existFlagElement)
                if (!existFlagElement) {
                  sectionPodTop.insertBefore(divFlag, sectionPodTop.firstChild)
                }
              })
            }

            if (timeValide.getHours() >= 7 && timeValide.getHours() < 17) {
              if (timeValide.getDay() <= 5) {
                if (event.data.delivery_in_24_hours){
                  event.data.delivery_in_24_hours.map(function(index) {
                    var divFlag = document.createElement('div')
                    divFlag.className = 'fb-flag-entrega--en-venticuadro-horas'
                    divFlag.innerHTML = htmlFlagInPLPAndPDPventi
                    var parentSkusDOM = document.querySelector('#fb-pod__item__input-'+ index).parentNode
                    var podItem = parentSkusDOM.parentNode
                    var sectionPodTop = podItem.querySelector('.pod-body .section__pod-top')
                    var existFlagElement = sectionPodTop.querySelector('.fb-flag-entrega--en-venticuadro-horas')
                    //console.log('existFlagElement',existFlagElement)
                    if (!existFlagElement) {
                      sectionPodTop.insertBefore(divFlag, sectionPodTop.firstChild)
                    }
                  })
                }
              }
            }
            


            
          }
        }

        var stopWorkerHastaLasTres = function () {
          w.terminate()
          w = undefined
        }

        var workerHastalasTres
        startWorkerHastaLasTres()

        var validateFlagOnScroll = function () {
          if (document.querySelectorAll('.fb-pod__item__compare__input')) {
            //console.log('queryStringHastaLasTres', queryStringHastaLasTres)
            if (queryStringHastaLasTres !== window.location.href) {
              queryStringHastaLasTres = window.location.href
              //console.log('on window.location.href' ,window.location.href)
              if (document.querySelectorAll('.fb-flag-entrega--en-venticuadro-horas').length <= 0){
                startWorkerHastaLasTres() 
              }
            }
          }
        }

        window.addEventListener('scroll', validateFlagOnScroll);

      //}
    //}

  }else{
    //Debugger
    ////console.log("Sorry, your browser does not support Web Workers...")
  }
}




//run code if Product Page
if (window.location.href.indexOf('/product/') != -1) {

  if (typeof Worker !== 'undefined') {
    var html_compra = ''
    function renderHtmlForFlagPDP (imageDesktopModal, imageImageModal, urlEnable, image) {
      return `
<div>
<style>
    .fb-product-cta__delivery-options{
        padding: 0;
        margin: 0;
        width: 33%;
    }
    .btn-shop {
        text-decoration: underline;
        text-align: center;
        color: #333;
    }
    .fb-add_content {
        float: left;
        display: flex;
        align-items: end;
    }

    .modal{
        max-width:960px;
    }

    .add_new-btn{
        float:left;
    }

    img {
        max-width: 100%;
    }

    .close, .close:visited{
        color:#333;
    }

    .recibe_icono{
        width: 35px; 
        height: 35px;
    }
    .fb-flag-entrega--en-venticuadro-horas.fb-in_PDP.fb-in_PDP--desktop{
      position: static;
      font-size: 1em;
      font-weight: 400;
      padding-bottom: 20px;
      width: 100%;
      padding: 0;
    }

    .fb-flag-entrega--en-venticuadro-horas.fb-in_PDP.fb-in_PDP--desktop .fb-img-express-recibe-entrega-hoy{
      width: 69% !important;
    }

    @media screen and (max-width: 768px) {
        .btn-shop {
            margin-top: 13px;
            margin-left: 0;
        }
        .modal{
            max-width: 414px;
            padding:0;
        }
        .fb-product-cta__delivery-options{{
   
        }
    }

    @media screen and (max-width: 414px){
        .fb-product-cta__delivery-options{
            padding: 0;
            margin: 0;
            width: 100% !important;
        }
    }
</style>
${urlEnable? `<div class="fb-product-cta__delivery-options add_new-btn">
    <a class="btn-shop">
      
      <img src="${image}" class="fb-img-express-recibe-entrega-hoy">
      <div class="fb-country-link" style="margin-left: 0px;text-align: left;">
            <span class="fb-link fb-bold-link" style="border: none;">Ver m&aacute;s</span>
            <div class="fb-element-arrow" style="margin-left: -6px;"></div>
        </div>
        
    </a>
</div>
<div class="tingle-demo tingle-demo-tiny" style="display: none;">
      <picture>
          <source srcset="${imageDesktopModal}" media="(min-width: 768px)">
          <img src="${imageImageModal}" alt="">
      </picture>
  </div>`: `<div class="fb-product-cta__delivery-options add_new-btn">
        ${image}

</div>
`}

<div>`;

    }  


    var jsFlagInPDP= document.createElement('script')
    jsFlagInPDP.type = 'application/javascript'
    jsFlagInPDP.async = true
    jsFlagInPDP.charset = 'UTF-8'
    jsFlagInPDP.onload = function () {
      var timeHastaLasTresPDP = new Date()
      if (true) {
        //timeHastaLasTresPDP.getHours() >= 7 && timeHastaLasTresPDP.getHours() < 16
        if (true) {
          //timeHastaLasTresPDP.getDay() <= 5
          var cssId = 'myCss'
          if (!document.getElementById(cssId)) {
            var head  = document.getElementsByTagName('head')[0]
            var link  = document.createElement('link')
            link.id   = cssId
            link.rel  = 'stylesheet'
            link.type = 'text/css'
            link.href = 'https://cdnjs.cloudflare.com/ajax/libs/tingle/0.14.0/tingle.min.css'
            link.media = 'all'
            head.appendChild(link)
          }

          var workerHastalasTres = new Worker(workerPath)
          var arraySkusHastalasTres = []
          var arraySkus = []
          var pathArray = window.location.pathname.split('/')
          if (pathArray[3]) {
            arraySkusHastalasTres.push(pathArray[3]);
            //console.log('hasta las tres', arraySkusHastalasTres)
            workerHastalasTres.postMessage({
              msg: arraySkusHastalasTres
            });
          }

          workerHastalasTres.onmessage = function(event) {
            //console.log('hasta las tres respuesta', event.data)
            var timeValide = new Date()
            var arrayFlagPDP = []
            if (event.data.delivery_today.length > 0) {
              console.log('event.data.delivery_today', event.data.delivery_today)
              arrayFlagPDP = event.data.delivery_today
              htmlFlagInPLPAndPDP = `/static/RDF/site/home/promohuincha/html/scripts/img/Icono_RecibeHoy.png" class="fb-img-express-recibe-entrega-hoy`
              html_compra = renderHtmlForFlagPDP('https://www.falabella.com.co/static/RDF/site/home/promohuincha/html/scripts/img/recibehoy/Recibehoy-desk.png', 'https://www.falabella.com.co/static/RDF/site/home/promohuincha/html/scripts/img/recibehoy/Recibehoy-mob.gif', true, htmlFlagInPLPAndPDP)
            }

            if (event.data.delivery_in_barranquilla_cali.length > 0) {
              console.log('event.data.delivery_in_barranquilla_cali', event.data.delivery_in_barranquilla_cali)
              arrayFlagPDP = event.data.delivery_in_barranquilla_cali
              htmlFlagInPLPAndPDP = `<img src="/static/RDF/site/home/promohuincha/html/scripts/img/ciudades/Entrega_Barranquilla_Cali.png" class="fb-img-express-recibe-entrega-hoy">`
              html_compra = renderHtmlForFlagPDP('https://www.falabella.com.co/static/RDF/site/home/promohuincha/html/scripts/img/recibehoy/Recibehoy-desk.png', 'https://www.falabella.com.co/static/RDF/site/home/promohuincha/html/scripts/img/recibehoy/Recibehoy-mob.gif', false, htmlFlagInPLPAndPDP)
            }

            if (event.data.delivery_in_barranquilla.length > 0) {
              console.log('event.data.delivery_in_barranquilla', event.data.delivery_in_barranquilla)
              arrayFlagPDP = event.data.delivery_in_barranquilla
              htmlFlagInPLPAndPDP = `<img src="/static/RDF/site/home/promohuincha/html/scripts/img/ciudades/Entrega_Barranquilla.png" class="fb-img-express-recibe-entrega-hoy">`
              html_compra = renderHtmlForFlagPDP('https://www.falabella.com.co/static/RDF/site/home/promohuincha/html/scripts/img/recibehoy/Recibehoy-desk.png', 'https://www.falabella.com.co/static/RDF/site/home/promohuincha/html/scripts/img/recibehoy/Recibehoy-mob.gif', false, htmlFlagInPLPAndPDP)
            }

            if (event.data.delivery_in_barranquilla_cali_medellin.length > 0) {
              console.log('event.data.delivery_in_barranquilla_cali_medellin', event.data.delivery_in_barranquilla_cali_medellin)
              arrayFlagPDP = event.data.delivery_in_barranquilla_cali_medellin
              htmlFlagInPLPAndPDP = `<img src="/static/RDF/site/home/promohuincha/html/scripts/img/ciudades/Entrega_Cali_Barra_Made.png" class="fb-img-express-recibe-entrega-hoy">`
              html_compra = renderHtmlForFlagPDP('https://www.falabella.com.co/static/RDF/site/home/promohuincha/html/scripts/img/recibehoy/Recibehoy-desk.png', 'https://www.falabella.com.co/static/RDF/site/home/promohuincha/html/scripts/img/recibehoy/Recibehoy-mob.gif', false, htmlFlagInPLPAndPDP)
            }

            if (event.data.delivery_in_cali_medellin.length > 0) {
              console.log('event.data.delivery_in_cali_medellin', event.data.delivery_in_cali_medellin)
              arrayFlagPDP = event.data.delivery_in_cali_medellin
              htmlFlagInPLPAndPDP = `<img src="/static/RDF/site/home/promohuincha/html/scripts/img/ciudades/Entrega_Cali_Medellin.png" class="fb-img-express-recibe-entrega-hoy">`
              html_compra = renderHtmlForFlagPDP('https://www.falabella.com.co/static/RDF/site/home/promohuincha/html/scripts/img/recibehoy/Recibehoy-desk.png', 'https://www.falabella.com.co/static/RDF/site/home/promohuincha/html/scripts/img/recibehoy/Recibehoy-mob.gif', false, htmlFlagInPLPAndPDP)
            }
            if (event.data.delivery_in_medellin.length > 0) {
              console.log('event.data.delivery_in_medellin', event.data.delivery_in_medellin)
              arrayFlagPDP = event.data.delivery_in_medellin
              htmlFlagInPLPAndPDP = `<img src="/static/RDF/site/home/promohuincha/html/scripts/img/ciudades/Entrega_Medellin.png" class="fb-img-express-recibe-entrega-hoy">`
              html_compra = renderHtmlForFlagPDP('https://www.falabella.com.co/static/RDF/site/home/promohuincha/html/scripts/img/recibehoy/Recibehoy-desk.png', 'https://www.falabella.com.co/static/RDF/site/home/promohuincha/html/scripts/img/recibehoy/Recibehoy-mob.gif', false, htmlFlagInPLPAndPDP)
            }
            if (event.data.delivery_in_barranquilla_medellin.length > 0) {
              console.log('event.data.delivery_in_barranquilla_medellin', event.data.delivery_in_barranquilla_medellin)
              arrayFlagPDP = event.data.delivery_in_barranquilla_medellin
              htmlFlagInPLPAndPDP =  `<img src="/static/RDF/site/home/promohuincha/html/scripts/img/ciudades/Entrega_Medellin_barra.png" class="fb-img-express-recibe-entrega-hoy">`
              html_compra = renderHtmlForFlagPDP('https://www.falabella.com.co/static/RDF/site/home/promohuincha/html/scripts/img/recibehoy/Recibehoy-desk.png', 'https://www.falabella.com.co/static/RDF/site/home/promohuincha/html/scripts/img/recibehoy/Recibehoy-mob.gif', false, htmlFlagInPLPAndPDP)
            }
            if (event.data.delivery_in_cali.length > 0) {
              console.log('event.data.delivery_in_cali', event.data.delivery_in_cali)
              arrayFlagPDP = event.data.delivery_in_cali
              htmlFlagInPLPAndPDP =  `<img src="/static/RDF/site/home/promohuincha/html/scripts/img/ciudades/Entrega_cali.png" class="fb-img-express-recibe-entrega-hoy">`
              html_compra = renderHtmlForFlagPDP('https://www.falabella.com.co/static/RDF/site/home/promohuincha/html/scripts/img/recibehoy/Recibehoy-desk.png', 'https://www.falabella.com.co/static/RDF/site/home/promohuincha/html/scripts/img/recibehoy/Recibehoy-mob.gif', false, htmlFlagInPLPAndPDP)
            }
            if (event.data.delivery_in_24_hours.length > 0) {
              html_compra = ''
              if (timeValide.getHours() >= 7 && timeValide.getHours() < 17) {
                if (timeValide.getDay() <= 5) {
                  console.log('event.data.delivery_in_24_hours', event.data.delivery_in_24_hours)
                  arrayFlagPDP = event.data.delivery_in_24_hours
                  htmlFlagInPLPAndPDP =  `/static/RDF/site/home/promohuincha/html/scripts/img/Icono_28Horas.png" class="fb-img-express-recibe-entrega-hoy`
                  html_compra = renderHtmlForFlagPDP('https://www.falabella.com.co/static/RDF/site/home/promohuincha/html/img/Recibehoy-desk31ago.png', 'https://www.falabella.com.co/static/RDF/site/home/promohuincha/html/img/Recibehoy-mob31ago.gif', true, htmlFlagInPLPAndPDP)
                }
              }
              
            }


            arrayFlagPDP.map(function(index) {
              if (window.location.href.indexOf("/" + index) != -1) {
                if (!document.querySelector('#fb-modal-entrega-venticuatrohoras')) {
                  var divModalFlag = document.createElement('div')
                  divModalFlag.id = 'fb-modal-entrega-venticuatrohoras'
                  //divModalFlag.innerHTML = html_compra

                  var divIconFlagMobile = document.createElement('div')
                  divIconFlagMobile.className = 'fb-flag-entrega--en-venticuadro-horas fb-in_PDP fb-in_PDP--mobile'
                  divIconFlagMobile.innerHTML = html_compra

                  var divIconFlagDesktop = document.createElement('div')
                  divIconFlagDesktop.className = 'fb-flag-entrega--en-venticuadro-horas fb-in_PDP fb-in_PDP--desktop'
                  divIconFlagDesktop.innerHTML = html_compra

                  var fbProductCtaControlsDelivery = document.querySelector('.fb-product-cta__controls--delivery')
                  fbProductCtaControlsDelivery.appendChild(divModalFlag)

                  var fbPriceListfbProductCtaPrices = document.querySelector('.fb-product-cta__container .fb-price-list.fb-product-cta__prices')
                  fbPriceListfbProductCtaPrices.append(divIconFlagMobile)

                  var fbProductCtaControlsDelivery = document.querySelector('.fb-product-cta__controls--delivery')
                  fbProductCtaControlsDelivery.insertBefore(divIconFlagDesktop, fbProductCtaControlsDelivery.firstChild)

                  var modalTinyNoFooter = new tingle.modal({
                      onClose: function() {
                          ////console.log('close')
                      },
                      onOpen: function() {
                          ////console.log('open')
                      },
                      beforeOpen: function() {
                          ////console.log('before open')
                      },
                      beforeClose: function() {
                          ////console.log('before close')
                          return true;
                      },
                      cssClass: ['class1', 'class2']
                  });
                  //var btn = document.querySelector('.btn-shop')
                  var btn = document.querySelectorAll('.btn-shop')
                  btn.forEach((data)=>{
                    data.addEventListener('click', function(){
                      modalTinyNoFooter.open()
                    })
                    modalTinyNoFooter.setContent(document.querySelector('.tingle-demo-tiny').innerHTML)
                  })
                  


                }
              }
            })
          }

        }
      } 
    }
    jsFlagInPDP.src = 'https://cdnjs.cloudflare.com/ajax/libs/tingle/0.14.0/tingle.min.js'
    document.body.appendChild(jsFlagInPDP)
  }
  
}

}



functionRecibeHoy()
